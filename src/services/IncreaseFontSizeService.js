export class IncreaseFontSizeService {

    arrayOfObservers = [];
    arrayForBackgroundColor = [];
    /*
    NOTE:  in this particular case, we will only ever have 1 observer.  however 
    we still need to use an array BECAUSE in the future it's very likely that we will
    have more than one observer.
    This array is actually just an array of functions(callbacks)
   
    */

    addObserversForFontChange = (callback) => {
        this.arrayOfObservers.push(callback);
    };

    addObserverForBackgroundColorChange = (callbackForColorChange) => {
        this.arrayForBackgroundColor.push(callbackForColorChange)
    }

    startNotifyingObserversOfFontChange = ()=> {
        setInterval(() => {
            this.arrayOfObservers.forEach((callback) => {
                console.log('here is your callback', callback);
                callback(this.changeSize())
            })
        }, 5000);
        /*
        NOTE:  the second parameter of setInterval is the time between firings
        */
    };
    

    notifyBackgroundColorChange = () => {
        this.arrayForBackgroundColor.forEach((something) => {
            something(this.changeBackgroundColor())
        })
    };


    changeBackgroundColor = () => {
        const colorChange = '#'+Math.floor(Math.random()*16777215).toString(16);
        console.log('changing backgroundcolor', colorChange);
        return colorChange;   
    }
    changeSize = () => {
        this.randomTestFunction();
        // console.log('inside change size');
        return Math.floor((this.generateRandomNumber() * 80) + 10);
    };

    generateRandomNumber = () => {
        return Math.random()
    };
    randomTestFunction = () => {
        // console.log('hey man..its working');
    }

    


    
    
}