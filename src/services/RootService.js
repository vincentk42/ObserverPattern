import {IncreaseFontSizeService} from './IncreaseFontSizeService';
/*
NOTE because we are NOT using export default IncreaseFontSizeService it's necessary to
use the curly braces when using import.  
*/
export class RootService {
    /*
    NOTE: remember to create a new instance by adding the ()!!
    */
    increaseFontSize = new IncreaseFontSizeService();

    constructor() {
        this.increaseFontSize.startNotifyingObserversOfFontChange()
    }
}