import { IncreaseFontSizeService } from "../IncreaseFontSizeService";

describe("IncreaseFontSizeService", function() {

    let service;

    beforeEach(function(){
        service = new IncreaseFontSizeService();
    });

    it("should confirm that changeSize gets fired", function () {
        spyOn(service, 'changeSize');
        // const hasBeen = spyOn(service, 'changeSize');
        //
        // service.changeSize();

        expect(service.changeSize()).toBe();
    });

    it("should confirm if any calls have been made", function() {
        spyOn(service, 'randomTestFunction');
        // service.changeSize.calls.any();

        service.changeSize();

        expect(service.randomTestFunction.calls.any()).toBe(true);
    });

    it("should confirm that the return value is greater than 10 for changeSize", function () {
        spyOn(service, 'generateRandomNumber').and.returnValues(0, 1);

        expect(service.changeSize()).toBeGreaterThanOrEqual(10);
        expect(service.changeSize()).toBeGreaterThanOrEqual(10);

    });

    it("should confirm that the font size is not greater than 90", function() {
        spyOn(service, 'generateRandomNumber').and.returnValues(0, 1);

        expect(service.changeSize()).toBeLessThanOrEqual(90);
        expect(service.changeSize()).toBeLessThanOrEqual(90);

    });
});

