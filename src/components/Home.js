import React, {Component} from 'react';
import {Text, View, TouchableOpacity } from 'react-native';

class Home extends Component {
    constructor() {
        super();
        this.state = {
            size: 10,
            backgroundColor: 'yellow',
        }
    }

    componentDidMount() {
        /*
        NOTE:  We do NOT need to import IncreaseFontSizeService because we already have access
        to it via screenProps.
        */
        this.props.screenProps.increaseFontSize.addObserversForFontChange(this.increaseFont);
    }

    increaseFont = (newFontSize) => {
        // console.log('here is your new font Size', newFontSize);
        this.setState({ size: newFontSize });
    };

    textPressed = () => {
        console.log('hey man..inside changebackground color');
        this.props.screenProps.increaseFontSize.addObserverForBackgroundColorChange(this.changeBackgroundColor)
        this.props.screenProps.increaseFontSize.notifyBackgroundColorChange();
    }
    changeBackgroundColor = (newColor) => {
        console.log('heres your new color', newColor);
        this.setState({
            backgroundColor: newColor
        })
    }
    render() {
        return(
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: this.state.backgroundColor

                
            }}>
            <Text style={{
                fontSize: this.state.size
            }}>Hello </Text>
            <TouchableOpacity
                onPress={() => this.textPressed()}
            >
                <Text>Press to change background Color</Text>
            </TouchableOpacity>
            </View>
        )
    }
}

export default Home;