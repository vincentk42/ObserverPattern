import React, {Component} from 'react';

import Home from './src/components/Home';
import { RootService } from './src/services/RootService';


export default class App extends Component {
  render() {
    return (
    <Home screenProps={ new RootService() } />
    );
  }
}
