# ObserverPattern

A simple app that uses the Observer Pattern to separate business logic from rendering.  
For the time being, the only thing this app does is randomly change the font size of "Hello World"

This now includes a second Observer(changeBackgroundColor).  I added this because the first time
I created this, I didn't realize that due to the polling nature of startNotifyingObserversOfFontChange
I never had to manually notify the subject.  In my original observer pattern, I used setInterval to fire
every 5 seconds to change the font size.  Because it was pinging every 5 seconds, I never had to worry
about the subject being notified.  

Now that I've added a manual button press to change the background color, I need to manually notify
the subject.  I decided the easiest/most logical way to do this was immediately after adding the observer.


This could have been done on a single page.  However, I wanted to better understand how a project 
could be built using the Observer Pattern.  I could not find any react native 
projects that used this pattern that I could use to learn. So I decided to make this tiny project.